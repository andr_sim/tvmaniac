package com.sevenander.tvmaniac.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.sevenander.tvmaniac.data.cache.TvShowCache;
import com.sevenander.tvmaniac.data.entity.mapper.TvShowEntityJsonMapper;
import com.sevenander.tvmaniac.data.net.RestApi;
import com.sevenander.tvmaniac.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link TvShowDataStore}.
 */
@Singleton
public class TvShowDataStoreFactory {

    private final Context context;
    private final TvShowCache userCache;

    @Inject
    public TvShowDataStoreFactory(@NonNull Context context, @NonNull TvShowCache userCache) {
        this.context = context.getApplicationContext();
        this.userCache = userCache;
    }

    /**
     * Create {@link TvShowDataStore} from a tv show link.
     */
    public TvShowDataStore create(String link) {
        TvShowDataStore userDataStore;

        if (!this.userCache.isExpired() && this.userCache.isCached(link)) {
            userDataStore = new DiskTvShowDataStore(this.userCache);
        } else {
            userDataStore = createCloudDataStore();
        }

        return userDataStore;
    }

    /**
     * Create {@link TvShowDataStore} to retrieve data from the Cloud.
     */
    public TvShowDataStore createCloudDataStore() {
        TvShowEntityJsonMapper userEntityJsonMapper = new TvShowEntityJsonMapper();
        RestApi restApi = new RestApiImpl(this.context, userEntityJsonMapper);

        return new CloudTvShowDataStore(restApi, this.userCache);
    }
}
