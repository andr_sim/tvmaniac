package com.sevenander.tvmaniac.data.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.sevenander.tvmaniac.data.entity.TvShowEntity;
import com.sevenander.tvmaniac.data.entity.mapper.TvShowEntityJsonMapper;
import com.sevenander.tvmaniac.data.exception.NetworkConnectionException;
import com.sevenander.tvmaniac.util.Logger;

import java.net.MalformedURLException;
import java.util.List;

import rx.Observable;

//import com.fernandocejas.frodo.annotation.RxLogObservable;

/**
 * {@link RestApi} implementation for retrieving data from the network.
 */
public class RestApiImpl implements RestApi {

    private final Context context;
    private final TvShowEntityJsonMapper tvShowEntityJsonMapper;

    /**
     * Constructor of the class
     *
     * @param context                {@link Context}.
     * @param tvShowEntityJsonMapper {@link TvShowEntityJsonMapper}.
     */
    public RestApiImpl(Context context, TvShowEntityJsonMapper tvShowEntityJsonMapper) {
        if (context == null || tvShowEntityJsonMapper == null) {
            throw new IllegalArgumentException("The constructor parameters cannot be null!!!");
        }
        this.context = context.getApplicationContext();
        this.tvShowEntityJsonMapper = tvShowEntityJsonMapper;
    }

    //    @RxLogObservable
    @Override
    public Observable<List<TvShowEntity>> userEntityList() {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    String responseUserEntities = getUserEntitiesFromApi();
                    Logger.d("sss=" + responseUserEntities);
                    if (responseUserEntities != null) {
                        subscriber.onNext(tvShowEntityJsonMapper.transformTvShowEntityCollection(
                                responseUserEntities));
                        subscriber.onCompleted();
                    } else {
                        subscriber.onError(new NetworkConnectionException());
                    }
                } catch (Exception e) {
                    subscriber.onError(new NetworkConnectionException(e.getCause()));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }

//  @RxLogObservable
//  @Override public Observable<UserEntity> userEntityById(final int userId) {
//    return Observable.create(subscriber -> {
//      if (isThereInternetConnection()) {
//        try {
//          String responseUserDetails = getUserDetailsFromApi(userId);
//          if (responseUserDetails != null) {
//            subscriber.onNext(tvShowEntityJsonMapper.transformUserEntity(responseUserDetails));
//            subscriber.onCompleted();
//          } else {
//            subscriber.onError(new NetworkConnectionException());
//          }
//        } catch (Exception e) {
//          subscriber.onError(new NetworkConnectionException(e.getCause()));
//        }
//      } else {
//        subscriber.onError(new NetworkConnectionException());
//      }
//    });
//  }

    private String getUserEntitiesFromApi() throws MalformedURLException {
        return ApiConnection.createGET(API_URL_GET_TV_SHOW_LIST).requestSyncCall();
    }

//    private String getUserDetailsFromApi(int userId) throws MalformedURLException {
//        String apiUrl = API_URL_GET_USER_DETAILS + userId + ".json";
//        return ApiConnection.createGET(apiUrl).requestSyncCall();
//    }

    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }
}
