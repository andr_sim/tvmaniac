package com.sevenander.tvmaniac.data.exception;

/**
 * Exception throw by the application when a TvShow search can't return a valid result.
 */
public class TvShowNotFoundException extends Exception {

    public TvShowNotFoundException() {
        super();
    }

    public TvShowNotFoundException(final String message) {
        super(message);
    }

    public TvShowNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public TvShowNotFoundException(final Throwable cause) {
        super(cause);
    }
}
