package com.sevenander.tvmaniac.data.repository.datasource;

import com.sevenander.tvmaniac.data.cache.TvShowCache;
import com.sevenander.tvmaniac.data.entity.TvShowEntity;
import com.sevenander.tvmaniac.data.net.RestApi;

import java.util.List;

import rx.Observable;
import rx.functions.Action1;

/**
 * {@link TvShowDataStore} implementation based on connections to the api (Cloud).
 */
class CloudTvShowDataStore implements TvShowDataStore {

    private final RestApi restApi;
    private final TvShowCache userCache;

    private final Action1<TvShowEntity> saveToCacheAction = userEntity -> {
        if (userEntity != null) {
            CloudTvShowDataStore.this.userCache.put(userEntity);
        }
    };

    /**
     * Construct a {@link TvShowDataStore} based on connections to the api (Cloud).
     *
     * @param restApi   The {@link RestApi} implementation to use.
     * @param userCache A {@link TvShowCache} to cache data retrieved from the api.
     */
    CloudTvShowDataStore(RestApi restApi, TvShowCache userCache) {
        this.restApi = restApi;
        this.userCache = userCache;
    }

    @Override
    public Observable<List<TvShowEntity>> tvShowEntityList() {
        return this.restApi.userEntityList();
    }

//    @Override
//    public Observable<TvShowEntity> userEntityDetails(final int userId) {
//        return this.restApi.userEntityById(userId).doOnNext(saveToCacheAction);
//    }
}
