package com.sevenander.tvmaniac.data.exception;

/**
 * Exception throw by the application when a Episode search can't return a valid result.
 */
public class EpisodeNotFoundException extends Exception {

    public EpisodeNotFoundException() {
        super();
    }

    public EpisodeNotFoundException(final String message) {
        super(message);
    }

    public EpisodeNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EpisodeNotFoundException(final Throwable cause) {
        super(cause);
    }
}
