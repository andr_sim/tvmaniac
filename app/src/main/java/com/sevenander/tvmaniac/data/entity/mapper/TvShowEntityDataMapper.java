/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sevenander.tvmaniac.data.entity.mapper;

import com.sevenander.tvmaniac.data.entity.TvShowEntity;
import com.sevenander.tvmaniac.domain.TvShow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link TvShowEntity} (in the data layer) to {@link TvShow} in the
 * domain layer.
 */
@Singleton
public class TvShowEntityDataMapper {

    @Inject
    public TvShowEntityDataMapper() {
    }

    /**
     * Transform a {@link TvShowEntity} into an {@link TvShow}.
     *
     * @param tvShowEntity Object to be transformed.
     * @return {@link TvShow} if valid {@link TvShowEntity} otherwise null.
     */
    public TvShow transform(TvShowEntity tvShowEntity) {
        TvShow tvShow = null;
        if (tvShowEntity != null) {
            tvShow = new TvShow();
            tvShow.setTitle(tvShowEntity.getTitle());
            StringBuilder poster = new StringBuilder(
                    tvShowEntity.getPoster().substring(0, tvShowEntity.getPoster().lastIndexOf("/")));
            poster = new StringBuilder(poster.substring(0, poster.lastIndexOf("/") + 1));
            poster.append("1")
                    .append(tvShowEntity.getPoster().substring(tvShowEntity.getPoster().lastIndexOf("/")));
            tvShow.setPosterUrl(String.format("http:%s", poster.toString()));
//      tvShow.setLink(tvShowEntity.getLink());
//      tvShow.setSection(tvShowEntity.getSection());
//      tvShow.setSubsection(tvShowEntity.getSubsection());
//      tvShow.setYear(tvShowEntity.getYear());
        }

        return tvShow;
    }

    /**
     * Transform a List of {@link TvShowEntity} into a Collection of {@link TvShow}.
     *
     * @param tvShowEntityCollection Object Collection to be transformed.
     * @return {@link TvShow} if valid {@link TvShowEntity} otherwise null.
     */
    public List<TvShow> transform(Collection<TvShowEntity> tvShowEntityCollection) {
        List<TvShow> tvShows = new ArrayList<>(20);
        TvShow tvShow;
        for (TvShowEntity tvShowEntity : tvShowEntityCollection) {
            tvShow = transform(tvShowEntity);
            if (tvShow != null) {
                tvShows.add(tvShow);
            }
        }

        return tvShows;
    }
}
