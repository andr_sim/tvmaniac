package com.sevenander.tvmaniac.data.entity.mapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

/**
 * Class used to transform from Strings representing json to valid objects.
 */
public class TvShowEntityJsonMapper {

    private final Gson gson;

    @Inject
    public TvShowEntityJsonMapper() {
        this.gson = new Gson();
    }

    /**
     * Transform from valid json string to {@link TvShowEntity}.
     *
     * @param tvShowJsonResponse A json representing a tv show search result.
     * @return {@link TvShowEntity}.
     * @throws JsonSyntaxException if the json string is not a valid json structure.
     */
    public TvShowEntity transformTvShowEntity(String tvShowJsonResponse) throws JsonSyntaxException {
        try {
            Type userEntityType = new TypeToken<TvShowEntity>() {
            }.getType();
            TvShowEntity userEntity = this.gson.fromJson(tvShowJsonResponse, userEntityType);

            return userEntity;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }

    /**
     * Transform from valid json string to List of {@link TvShowEntity}.
     *
     * @param tvShowListJsonResponse A json representing a collection of tv shows.
     * @return List of {@link TvShowEntity}.
     * @throws JsonSyntaxException if the json string is not a valid json structure.
     */
    public List<TvShowEntity> transformTvShowEntityCollection(String tvShowListJsonResponse)
            throws JsonSyntaxException {

        List<TvShowEntity> userEntityCollection;
        try {
            Type listOfTvShowEntityType = new TypeToken<List<TvShowEntity>>() {
            }.getType();
            userEntityCollection = this.gson.fromJson(tvShowListJsonResponse, listOfTvShowEntityType);

            return userEntityCollection;
        } catch (JsonSyntaxException jsonException) {
            throw jsonException;
        }
    }
}
