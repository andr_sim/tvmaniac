package com.sevenander.tvmaniac.data.net;

import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import java.util.List;

import rx.Observable;

/**
 * RestApi for retrieving data from the network.
 */
public interface RestApi {
    //    String API_BASE_URL = "http://www.android10.org/myapi/";
    String API_BASE_URL = "http://fs.to/";

    /**
     * Api url for getting all tvShows
     */
//    String API_URL_GET_USER_LIST = API_BASE_URL + "tvShows.json";
    String API_URL_GET_TV_SHOW_LIST = API_BASE_URL + "search.aspx?f=quick_search&search=arrow";
    /**
     * Api url for getting a tvShow profile: Remember to concatenate id + 'json'
     */
//    String API_URL_GET_USER_DETAILS = API_BASE_URL + "user_";

    /**
     * Retrieves an {@link rx.Observable} which will emit a List of {@link TvShowEntity}.
     */
    Observable<List<TvShowEntity>> userEntityList();

    /**
     * Retrieves an {@link rx.Observable} which will emit a {@link UserEntity}.
     *
     * @param userId The tvShow id used to get tvShow data.
     */
//  Observable<UserEntity> userEntityById(final int userId);
}
