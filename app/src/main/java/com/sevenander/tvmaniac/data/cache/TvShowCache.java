package com.sevenander.tvmaniac.data.cache;

import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import rx.Observable;

/**
 * An interface representing a tv show Cache.
 */
public interface TvShowCache {
    /**
     * Gets an {@link Observable} which will emit a {@link TvShowEntity}.
     *
     * @param link The tv show link to retrieve data.
     */
    Observable<TvShowEntity> get(final String link);

    /**
     * Puts and element into the cache.
     *
     * @param tvShowEntity Element to insert in the cache.
     */
    void put(TvShowEntity tvShowEntity);

    /**
     * Checks if an element (TvShow) exists in the cache.
     *
     * @param link The id used to look for inside the cache.
     * @return true if the element is cached, otherwise false.
     */
    boolean isCached(final String link);

    /**
     * Checks if the cache is expired.
     *
     * @return true, the cache is expired, otherwise false.
     */
    boolean isExpired();

    /**
     * Evict all elements of the cache.
     */
    void evictAll();
}
