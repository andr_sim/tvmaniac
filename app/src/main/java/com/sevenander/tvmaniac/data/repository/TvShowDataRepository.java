package com.sevenander.tvmaniac.data.repository;

import com.sevenander.tvmaniac.data.entity.mapper.TvShowEntityDataMapper;
import com.sevenander.tvmaniac.data.repository.datasource.TvShowDataStore;
import com.sevenander.tvmaniac.data.repository.datasource.TvShowDataStoreFactory;
import com.sevenander.tvmaniac.domain.TvShow;
import com.sevenander.tvmaniac.domain.repository.TvShowRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * {@link TvShowRepository} for retrieving user data.
 */
@Singleton
public class TvShowDataRepository implements TvShowRepository {

    private final TvShowDataStoreFactory tvShowDataStoreFactory;
    private final TvShowEntityDataMapper tvShowEntityDataMapper;

    /**
     * Constructs a {@link TvShowRepository}.
     *
     * @param dataStoreFactory       A factory to construct different data source implementations.
     * @param tvShowEntityDataMapper {@link TvShowEntityDataMapper}.
     */
    @Inject
    public TvShowDataRepository(TvShowDataStoreFactory dataStoreFactory,
                                TvShowEntityDataMapper tvShowEntityDataMapper) {
        this.tvShowDataStoreFactory = dataStoreFactory;
        this.tvShowEntityDataMapper = tvShowEntityDataMapper;
    }

    @Override
    public Observable<List<TvShow>> tvShows() {
        //we always get all users from the cloud
        final TvShowDataStore tvShowDataStore = this.tvShowDataStoreFactory.createCloudDataStore();
        return tvShowDataStore.tvShowEntityList().map(this.tvShowEntityDataMapper::transform);
    }

//    @Override
//    public Observable<TvShow> tvShow(int userId) {
//        final TvShowDataStore tvShowDataStore = this.tvShowDataStoreFactory.create(userId);
//        return tvShowDataStore.userEntityDetails(userId).map(this.tvShowEntityDataMapper::transform);
//    }
}
