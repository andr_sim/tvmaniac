package com.sevenander.tvmaniac.data.repository.datasource;

import com.sevenander.tvmaniac.data.cache.TvShowCache;
import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import java.util.List;

import rx.Observable;

/**
 * {@link TvShowDataStore} implementation based on file system data store.
 */
class DiskTvShowDataStore implements TvShowDataStore {

    private final TvShowCache tvShowCache;

    /**
     * Construct a {@link TvShowDataStore} based file system data store.
     *
     * @param tvShowCache A {@link TvShowCache} to cache data retrieved from the api.
     */
    DiskTvShowDataStore(TvShowCache tvShowCache) {
        this.tvShowCache = tvShowCache;
    }

    @Override
    public Observable<List<TvShowEntity>> tvShowEntityList() {
        //TODO: implement simple cache for storing/retrieving collections of users.
        throw new UnsupportedOperationException("Operation is not available!!!");
    }

//    @Override
//    public Observable<TvShowEntity> tvShowEntityDetails(final String link) {
//        return this.tvShowCache.get(link);
//    }
}
