package com.sevenander.tvmaniac.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * TvShow Entity used in the data layer.
 */
public class TvShowEntity {

    @SerializedName("title")
    private String title;

    @SerializedName("section")
    private String section;

    @SerializedName("subsection")
    private String subsection;

    @SerializedName("link")
    private String link;

    @SerializedName("poster")
    private String poster;

    @SerializedName("year")
    private List<String> year;

    public TvShowEntity() {
        //empty
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSubsection() {
        return subsection;
    }

    public void setSubsection(String subsection) {
        this.subsection = subsection;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }
}
