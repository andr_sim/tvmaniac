package com.sevenander.tvmaniac.data.repository.datasource;

import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import java.util.List;

import rx.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface TvShowDataStore {
    /**
     * Get an {@link Observable} which will emit a List of {@link TvShowEntity}.
     */
    Observable<List<TvShowEntity>> tvShowEntityList();

    /**
     * Get an {@link Observable} which will emit a {@link TvShowEntity} by its id.
     *
     * @param userId The id to retrieve user data.
     */
//  Observable<TvShowEntity> tvShowEntityDetails(final int userId);
}
