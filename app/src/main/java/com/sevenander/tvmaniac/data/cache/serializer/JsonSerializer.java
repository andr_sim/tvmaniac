package com.sevenander.tvmaniac.data.cache.serializer;

import com.google.gson.Gson;
import com.sevenander.tvmaniac.data.entity.TvShowEntity;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Class user as Serializer/Deserializer for user entities.
 */
@Singleton
public class JsonSerializer {

    private final Gson gson = new Gson();

    @Inject
    public JsonSerializer() {
    }

    /**
     * Serialize an object to Json.
     *
     * @param tvShowEntity {@link TvShowEntity} to serialize.
     */
    public String serialize(TvShowEntity tvShowEntity) {
        String jsonString = gson.toJson(tvShowEntity, TvShowEntity.class);
        return jsonString;
    }

    /**
     * Deserialize a json representation of an object.
     *
     * @param jsonString A json string to deserialize.
     * @return {@link TvShowEntity}
     */
    public TvShowEntity deserialize(String jsonString) {
        TvShowEntity tvShowEntity = gson.fromJson(jsonString, TvShowEntity.class);
        return tvShowEntity;
    }
}
