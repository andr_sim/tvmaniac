package com.sevenander.tvmaniac.domain;

/**
 * Class that represents a TvShow in the domain layer.
 */
public class TvShow {

    private int tvShowId;
    private String posterUrl;
    private String title;
    private boolean isWatching;

    public TvShow() {
    }

    public TvShow(int tvShowId) {
        this.tvShowId = tvShowId;
    }

    public int getTvShowId() {
        return tvShowId;
    }

    public void setTvShowId(int tvShowId) {
        this.tvShowId = tvShowId;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isWatching() {
        return isWatching;
    }

    public void setIsWatching(boolean isWatching) {
        this.isWatching = isWatching;
    }
}
