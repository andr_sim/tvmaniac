package com.sevenander.tvmaniac.domain;

import com.sevenander.tvmaniac.presentation.model.TranslationLanguage;

import java.util.Date;

/**
 * Created by andrii on 21.10.16.
 */
public class SeasonTranslation extends SeasonItem {

    private int episodeCount;
    private TranslationLanguage language;
    private Date lastUpdate;

    public int getEpisodeCount() {
        return episodeCount;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public TranslationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(TranslationLanguage language) {
        this.language = language;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public int getType() {
        return TYPE_ITEM_DEFAULT;
    }
}
