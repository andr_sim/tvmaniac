/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sevenander.tvmaniac.domain.interactor;

import com.sevenander.tvmaniac.domain.TvShow;
import com.sevenander.tvmaniac.domain.executor.PostExecutionThread;
import com.sevenander.tvmaniac.domain.executor.ThreadExecutor;
import com.sevenander.tvmaniac.domain.repository.TvShowRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * This class is an implementation of {@link UseCase} that represents a use case for
 * retrieving a collection of all {@link TvShow}.
 */
public class GetTvShowList extends UseCase {

    private final TvShowRepository tvShowRepository;

    @Inject
    public GetTvShowList(TvShowRepository tvShowRepository, ThreadExecutor threadExecutor,
                         PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.tvShowRepository = tvShowRepository;
    }

    @Override
    public Observable buildUseCaseObservable() {
        return this.tvShowRepository.tvShows();
    }
}
