package com.sevenander.tvmaniac.domain.repository;

import com.sevenander.tvmaniac.domain.TvShow;

import java.util.List;

import rx.Observable;

/**
 * Interface that represents a Repository for getting {@link TvShow} related data.
 */
public interface TvShowRepository {
    /**
     * Get an {@link Observable} which will emit a List of {@link TvShow}.
     */
    Observable<List<TvShow>> tvShows();

    /**
     * Get an {@link Observable} which will emit a {@link TvShow}.
     *
     * @param userId The tvShow id used to retrieve tvShow data.
     */
//    Observable<TvShow> tvShow(final int userId);
}
