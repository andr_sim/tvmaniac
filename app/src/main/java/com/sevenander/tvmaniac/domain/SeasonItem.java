package com.sevenander.tvmaniac.domain;

/**
 * Created by andrii on 21.10.16.
 */
public abstract class SeasonItem {

    public static final int TYPE_ITEM_TOP = 1;
    public static final int TYPE_ITEM_DEFAULT = 2;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public abstract int getType();
}
