package com.sevenander.tvmaniac.util;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.TranslationLanguage;
import com.sevenander.tvmaniac.presentation.model.VideoQuality;

/**
 * Created by andrii on 21.10.16.
 */
public class Utils {

    public static int getQualityColor(VideoQuality quality) {
        switch (quality) {
            case QUALITY_1080P:
                return R.color.indigo;
            case QUALITY_720P:
                return R.color.indigo;
            case QUALITY_WEBDL:
                return R.color.blue;
            case QUALITY_HDTVRIP:
                return R.color.blue;
            case QUALITY_HDRIP:
                return R.color.teal;
            case QUALITY_480P:
                return R.color.green;
            case QUALITY_360P:
                return R.color.orange;
            case QUALITY_SD:
                return R.color.deep_orange;
            default:
                return R.color.blue_grey;
        }
    }

    public static int getQualityColorDark(VideoQuality quality) {
        switch (quality) {
            case QUALITY_1080P:
                return R.color.indigo_dark;
            case QUALITY_720P:
                return R.color.indigo_dark;
            case QUALITY_WEBDL:
                return R.color.blue_dark;
            case QUALITY_HDTVRIP:
                return R.color.blue_dark;
            case QUALITY_HDRIP:
                return R.color.teal_dark;
            case QUALITY_480P:
                return R.color.green_dark;
            case QUALITY_360P:
                return R.color.orange_dark;
            case QUALITY_SD:
                return R.color.deep_orange_dark;
            default:
                return R.color.blue_grey_dark;
        }
    }

    public static int getDrawableFlag(TranslationLanguage lang) {
        switch (lang) {
            case UKR:
                return R.drawable.ukraine;
            case RUS:
                return R.drawable.germany;
            default:
                return R.drawable.usa;
        }
    }
}
