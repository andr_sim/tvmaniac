package com.sevenander.tvmaniac.presentation.view.fragment.callbacks;

import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;

public interface SeasonTranslationListListener {
    void onSeasonTranslationClicked(SeasonTranslationModel seasonTranslationModel);
}
