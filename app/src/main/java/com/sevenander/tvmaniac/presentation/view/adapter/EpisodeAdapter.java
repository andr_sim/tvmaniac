package com.sevenander.tvmaniac.presentation.view.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.EpisodeModel;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.EpisodeItemClickListener;
import com.sevenander.tvmaniac.presentation.view.adapter.holders.EpisodeViewHolder;
import com.sevenander.tvmaniac.util.Utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class EpisodeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    private List<EpisodeModel> episodeModels;
    private final LayoutInflater layoutInflater;

    private EpisodeItemClickListener listener;

    @Inject
    public EpisodeAdapter(Context context) {
        this.context = context;
        this.episodeModels = Collections.emptyList();
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return (episodeModels != null) ? episodeModels.size() : 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = layoutInflater.inflate(R.layout.item_episode, parent, false);
        return new EpisodeViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EpisodeViewHolder episodeHolder = (EpisodeViewHolder) holder;

        EpisodeModel episodeModel = episodeModels.get(position);

        episodeHolder.tvTitle.setText(String.format("Episode %d", episodeModel.getEpisodeNumber()));
        GradientDrawable bgShape = (GradientDrawable) episodeHolder.tvQuality.getBackground();
        bgShape.setColor(
                context.getResources().getColor(Utils.getQualityColor(episodeModel.getQuality())));
        episodeHolder.tvFile.setText(episodeModel.getDirectFileName());
        episodeHolder.tvQuality.setText(
                episodeModel.getQuality().name().replace("QUALITY_", "").toLowerCase());
    }

    public void setEpisodeModels(Collection<EpisodeModel> episodeModels) {
        validateEpisodesCollection(episodeModels);
        this.episodeModels = (List<EpisodeModel>) episodeModels;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(EpisodeItemClickListener listener) {
        this.listener = listener;
    }

    private void validateEpisodesCollection(Collection<EpisodeModel> episodeModels) {
        if (episodeModels == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }
}
