package com.sevenander.tvmaniac.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TvShowItemClickListener;
import com.sevenander.tvmaniac.presentation.view.adapter.holders.TvShowViewHolder;
import com.sevenander.tvmaniac.util.ImageUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class TvShowAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TvShowModel> tvShowModels;
    private final LayoutInflater layoutInflater;

    private TvShowItemClickListener listener;

    @Inject
    public TvShowAdapter(Context context) {
        this.tvShowModels = Collections.emptyList();
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return (tvShowModels != null) ? tvShowModels.size() : 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View view = layoutInflater.inflate(R.layout.item_tv_show, parent, false);
        TvShowViewHolder holder = new TvShowViewHolder(view, listener);
        calcItemSize(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TvShowViewHolder tvShowHolder = (TvShowViewHolder) holder;
        TvShowModel tvShowModel = tvShowModels.get(position);
        tvShowHolder.setTvShowModel(tvShowModel);
        ImageUtils.setImage(tvShowModel.getPosterUrl(), tvShowHolder.ivShowPoster);
        tvShowHolder.tvShowTitle.setText(tvShowModel.getTitle());
    }

    public void setTvShowModels(Collection<TvShowModel> tvShowModels) {
        validateEpisodesCollection(tvShowModels);
        this.tvShowModels = (List<TvShowModel>) tvShowModels;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(TvShowItemClickListener listener) {
        this.listener = listener;
    }

    private void validateEpisodesCollection(Collection<TvShowModel> episodeModels) {
        if (episodeModels == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    private void calcItemSize(final TvShowViewHolder holder) {
        ViewTreeObserver vto = holder.itemView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                holder.itemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int size = holder.itemView.getMeasuredWidth();

                int h = 3 * (size / 2);
                holder.rlShowPosterContainer.getLayoutParams().height = h;
                holder.ivShowPoster.getLayoutParams().height = h;
                holder.rlShowPosterContainer.requestLayout();
            }
        });
    }
}
