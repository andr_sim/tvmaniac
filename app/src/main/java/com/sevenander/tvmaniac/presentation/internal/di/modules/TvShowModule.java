package com.sevenander.tvmaniac.presentation.internal.di.modules;

import com.sevenander.tvmaniac.domain.interactor.GetTvShowList;
import com.sevenander.tvmaniac.domain.interactor.UseCase;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides tv show related collaborators.
 */
@Module
public class TvShowModule {

    private int tvShowId = -1;

    public TvShowModule() {
    }

    public TvShowModule(int tvShowId) {
        this.tvShowId = tvShowId;
    }

    @Provides
    @PerActivity
    @Named("tvShowList")
    UseCase provideGetUserListUseCase(GetTvShowList getTvShowList) {
        return getTvShowList;
    }
}
