package com.sevenander.tvmaniac.presentation.view.adapter.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TvShowItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TvShowViewHolder extends RecyclerView.ViewHolder {

    protected TvShowItemClickListener listener;

    @Bind(R.id.rl_item_show_poster) public RelativeLayout rlShowPosterContainer;
    @Bind(R.id.iv_item_show_poster) public SelectableRoundedImageView ivShowPoster;
    @Bind(R.id.tv_item_show_title) public TextView tvShowTitle;
    @Bind(R.id.tv_item_episode_title) public TextView tvEpisodeTitle;
    @Bind(R.id.b_item_episode_options) public ImageButton bEpisodeOptions;

    private TvShowModel tvShowModel;

    public TvShowViewHolder(View itemView, TvShowItemClickListener listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
    }

    public void setTvShowModel(TvShowModel tvShowModel) {
        this.tvShowModel = tvShowModel;
    }

    @OnClick(R.id.cv_tv_show)
    public void onItemClick() {
        if (listener != null)
            listener.onItemClick(tvShowModel);
    }

    @OnClick(R.id.b_item_episode_options)
    public void onItemMoreClick() {
        if (listener != null)
            listener.onItemMoreClick(bEpisodeOptions, getAdapterPosition());
    }
}
