package com.sevenander.tvmaniac.presentation.view;

import com.sevenander.tvmaniac.presentation.model.TvShowModel;

import java.util.Collection;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a list of {@link TvShowModel}.
 */
public interface TvShowListView extends LoadDataView {
    /**
     * Render a tv show list in the UI.
     *
     * @param tvShowModels The collection of {@link TvShowModel} that will be shown.
     */
    void renderTvShowList(Collection<TvShowModel> tvShowModels);

    /**
     * View a {@link TvShowModel} details.
     *
     * @param tvShowModel The tv show that will be shown.
     */
    void viewTvShow(TvShowModel tvShowModel);
}
