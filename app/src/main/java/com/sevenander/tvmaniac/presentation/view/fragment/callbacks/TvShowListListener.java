package com.sevenander.tvmaniac.presentation.view.fragment.callbacks;

import com.sevenander.tvmaniac.presentation.model.TvShowModel;

public interface TvShowListListener {
    void onTvShowClicked(TvShowModel tvShowModel);
}
