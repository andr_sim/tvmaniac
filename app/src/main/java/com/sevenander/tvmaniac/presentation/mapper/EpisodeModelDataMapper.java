package com.sevenander.tvmaniac.presentation.mapper;

import com.sevenander.tvmaniac.domain.Episode;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.model.EpisodeModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

/**
 * Mapper class used to transform {@link Episode} (in the domain layer) to {@link EpisodeModel}
 * in the presentation layer.
 */
@PerActivity
public class EpisodeModelDataMapper {

    @Inject
    public EpisodeModelDataMapper() {
    }

    /**
     * Transform a {@link Episode} into an {@link EpisodeModel}.
     *
     * @param episode Object to be transformed.
     * @return {@link EpisodeModel}.
     */
    public EpisodeModel transform(Episode episode) {
        if (episode == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        EpisodeModel userModel = new EpisodeModel(episode.getId());
        userModel.setDirectFileUrl(episode.getDirectFileUrl());
        userModel.setDirectFileName(episode.getDirectFileName());
        userModel.setDirectFileSize(episode.getDirectFileSize());
        userModel.setEpisodeNumber(episode.getEpisodeNumber());
        userModel.setQuality(episode.getQuality());
        userModel.setRequestLink(episode.getRequestLink());
        userModel.setStreamLink(episode.getStreamLink());

        return userModel;
    }

    /**
     * Transform a Collection of {@link Episode} into a Collection of {@link EpisodeModel}.
     *
     * @param episodeCollection Objects to be transformed.
     * @return List of {@link EpisodeModel}.
     */
    public Collection<EpisodeModel> transform(Collection<Episode> episodeCollection) {
        Collection<EpisodeModel> episodeModelCollection;

        if (episodeCollection != null && !episodeCollection.isEmpty()) {
            episodeModelCollection = new ArrayList<>();
            for (Episode episode : episodeCollection) {
                episodeModelCollection.add(transform(episode));
            }
        } else {
            episodeModelCollection = Collections.emptyList();
        }

        return episodeModelCollection;
    }
}
