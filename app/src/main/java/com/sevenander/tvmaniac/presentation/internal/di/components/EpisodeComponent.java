package com.sevenander.tvmaniac.presentation.internal.di.components;

import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.internal.di.modules.ActivityModule;
import com.sevenander.tvmaniac.presentation.internal.di.modules.EpisodeModule;
import com.sevenander.tvmaniac.presentation.view.fragment.EpisodeListFragment;

import dagger.Component;

/**
 * A scope {@link PerActivity} component.
 * Injects episode specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, EpisodeModule.class})
public interface EpisodeComponent extends ActivityComponent {
    void inject(EpisodeListFragment userListFragment);
//  void inject(UserDetailsFragment userDetailsFragment);
}
