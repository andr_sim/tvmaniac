package com.sevenander.tvmaniac.presentation.mapper;

import com.sevenander.tvmaniac.domain.TvShow;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

/**
 * Mapper class used to transform {@link TvShow} (in the domain layer) to {@link TvShowModel}
 * in the presentation layer.
 */
@PerActivity
public class TvShowModelDataMapper {

    @Inject
    public TvShowModelDataMapper() {
    }

    /**
     * Transform a {@link TvShow} into an {@link TvShowModel}.
     *
     * @param tvShow Object to be transformed.
     * @return {@link TvShowModel}.
     */
    public TvShowModel transform(TvShow tvShow) {
        if (tvShow == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        TvShowModel tvShowModel = new TvShowModel(tvShow.getTvShowId());
        tvShowModel.setTitle(tvShow.getTitle());
        tvShowModel.setPosterUrl(tvShow.getPosterUrl());
        tvShowModel.setIsWatching(tvShow.isWatching());

        return tvShowModel;
    }

    /**
     * Transform a Collection of {@link TvShow} into a Collection of {@link TvShowModel}.
     *
     * @param tvShows Objects to be transformed.
     * @return List of {@link TvShowModel}.
     */
    public Collection<TvShowModel> transform(Collection<TvShow> tvShows) {
        Collection<TvShowModel> tvShowModels;

        if (tvShows != null && !tvShows.isEmpty()) {
            tvShowModels = new ArrayList<>();
            for (TvShow tvShow : tvShows) {
                tvShowModels.add(transform(tvShow));
            }
        } else {
            tvShowModels = Collections.emptyList();
        }

        return tvShowModels;
    }
}
