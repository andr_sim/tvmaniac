package com.sevenander.tvmaniac.presentation.view.adapter.callbacks;

/**
 * Created by andrii on 21.10.16.
 */
public interface EpisodeItemClickListener {
    void onItemClick(int position);

    void onItemDownloadClick(int position);
}
