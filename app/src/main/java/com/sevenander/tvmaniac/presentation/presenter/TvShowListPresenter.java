package com.sevenander.tvmaniac.presentation.presenter;

import android.support.annotation.NonNull;

import com.sevenander.tvmaniac.domain.TvShow;
import com.sevenander.tvmaniac.domain.exception.ErrorBundle;
import com.sevenander.tvmaniac.domain.interactor.UseCase;
import com.sevenander.tvmaniac.presentation.exception.ErrorMessageFactory;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.mapper.TvShowModelDataMapper;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.presenter.subscriber.TvShowListSubscriber;
import com.sevenander.tvmaniac.presentation.view.TvShowListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class TvShowListPresenter implements Presenter {

    private TvShowListView viewListView;

    private final UseCase getTvShowListUseCase;
    private final TvShowModelDataMapper tvShowModelDataMapper;

    @Inject
    public TvShowListPresenter(@Named("tvShowList") UseCase getTvShowListUseCase,
                               TvShowModelDataMapper tvShowModelDataMapper) {
        this.getTvShowListUseCase = getTvShowListUseCase;
        this.tvShowModelDataMapper = tvShowModelDataMapper;
    }

    public void setView(@NonNull TvShowListView view) {
        this.viewListView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.getTvShowListUseCase.unsubscribe();
        this.viewListView = null;
    }

    /**
     * Initializes the presenter by start retrieving the tvShow list.
     */
    public void initialize() {
        this.loadTvShowList();
    }

    /**
     * Loads all tvShows.
     */
    private void loadTvShowList() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getTvShowList();
    }

    public void onTvShowClicked(TvShowModel tvShowModel) {
        this.viewListView.viewTvShow(tvShowModel);
    }

    private void showViewLoading() {
        this.viewListView.showLoading();
    }

    public void hideViewLoading() {
        this.viewListView.hideLoading();
    }

    public void showViewRetry() {
        this.viewListView.showRetry();
    }

    private void hideViewRetry() {
        this.viewListView.hideRetry();
    }

    public void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
                errorBundle.getException());
        this.viewListView.showError(errorMessage);
    }

    public void showTvShowsCollectionInView(Collection<TvShow> tvShows) {
        final Collection<TvShowModel> tvShowModels = this.tvShowModelDataMapper.transform(tvShows);
        this.viewListView.renderTvShowList(tvShowModels);
    }

    private void getTvShowList() {
//        hideViewLoading();
//        showTvShowsCollectionInView(mockTvShowList());
        getTvShowListUseCase.execute(new TvShowListSubscriber(TvShowListPresenter.this));
    }

    private List<TvShow> mockTvShowList() {
        List<TvShow> tvShows = new ArrayList<>();

        TvShow tvShow = new TvShow();
        tvShow.setTitle("Westworld");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/64/10/00426418.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("The Big Bang Theory");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/61/10/00426156.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("Supernatural");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/66/10/00426696.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("The Flash");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/64/10/00426478.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("Elementary");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/64/10/00426459.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("Timeless");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/64/10/00426464.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("Arrow");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/65/10/00426507.jpg");
        tvShows.add(tvShow);

        tvShow = new TvShow();
        tvShow.setTitle("Agents of S.H.I.E.L.D");
        tvShow.setPosterUrl("http://img.dotua.org/fsua_items/cover/00/42/61/10/00426106.jpg");
        tvShows.add(tvShow);

//        Collections.sort(lessons, new LessonComparator());
        return tvShows;
    }
}
