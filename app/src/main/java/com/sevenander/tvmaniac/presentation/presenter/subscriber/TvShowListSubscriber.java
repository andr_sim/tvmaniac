package com.sevenander.tvmaniac.presentation.presenter.subscriber;

import com.sevenander.tvmaniac.domain.TvShow;
import com.sevenander.tvmaniac.domain.exception.DefaultErrorBundle;
import com.sevenander.tvmaniac.domain.interactor.DefaultSubscriber;
import com.sevenander.tvmaniac.presentation.presenter.TvShowListPresenter;

import java.util.List;

public class TvShowListSubscriber extends DefaultSubscriber<List<TvShow>> {

    private TvShowListPresenter tvShowListPresenter;

    public TvShowListSubscriber(TvShowListPresenter tvShowListPresenter) {
        this.tvShowListPresenter = tvShowListPresenter;
    }

    @Override
    public void onCompleted() {
        tvShowListPresenter.hideViewLoading();
    }

    @Override
    public void onError(Throwable e) {
        tvShowListPresenter.hideViewLoading();
        tvShowListPresenter.showErrorMessage(new DefaultErrorBundle((Exception) e));
        tvShowListPresenter.showViewRetry();
    }

    @Override
    public void onNext(List<TvShow> tvShows) {
        tvShowListPresenter.showTvShowsCollectionInView(tvShows);
    }
}
