package com.sevenander.tvmaniac.presentation.model;

/**
 * Class that represents a TvShow in the presentation layer.
 */
public class TvShowModel {

    private int tvShowId;
    private String posterUrl;
    private String title;
    private boolean isWatching;

    public TvShowModel() {
    }

    public TvShowModel(int tvShowId) {
        this.tvShowId = tvShowId;
    }

    public int getTvShowId() {
        return tvShowId;
    }

    public void setTvShowId(int tvShowId) {
        this.tvShowId = tvShowId;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isWatching() {
        return isWatching;
    }

    public void setIsWatching(boolean isWatching) {
        this.isWatching = isWatching;
    }
}
