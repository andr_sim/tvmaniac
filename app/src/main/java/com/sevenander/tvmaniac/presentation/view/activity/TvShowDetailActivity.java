package com.sevenander.tvmaniac.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.HasComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.DaggerSeasonItemComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.SeasonItemComponent;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.view.fragment.SeasonItemListFragment;
import com.sevenander.tvmaniac.presentation.view.fragment.callbacks.SeasonTranslationListListener;
import com.sevenander.tvmaniac.util.ImageUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TvShowDetailActivity extends BaseActivity implements HasComponent<SeasonItemComponent>,
        SeasonTranslationListListener,
        AppBarLayout.OnOffsetChangedListener {

    @Bind(R.id.app_bar) AppBarLayout appBarLayout;
    @Bind(R.id.backdrop) ImageView ivBackdrop;
    @Bind(R.id.toolbar) Toolbar toolbar;

    private SeasonItemComponent seasonItemComponent;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, TvShowDetailActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_detail);
        ButterKnife.bind(this);
        initializeInjector();
        setupAppBar();
        setupToolbar();

        replaceFragment(R.id.rl_container, new SeasonItemListFragment());
    }

    private void initializeInjector() {
        this.seasonItemComponent = DaggerSeasonItemComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public SeasonItemComponent getComponent() {
        return seasonItemComponent;
    }

    private void setupAppBar() {
        appBarLayout.addOnOffsetChangedListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle(getIntent().getExtras().getString("title"));// todo change extras handling
        ImageUtils.setImage(getIntent().getExtras().getString("image"), ivBackdrop);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int totalScroll = appBarLayout.getTotalScrollRange();
        int currentScroll = totalScroll + verticalOffset;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int colorId;
            if ((currentScroll) < 255) {
                colorId = R.color.colorPrimary;
            } else {
                colorId = android.R.color.transparent;
            }
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(colorId));
        }
    }

    @Override
    public void onSeasonTranslationClicked(SeasonTranslationModel seasonTranslationModel) {
        navigator.navigateToTranslationDetails(TvShowDetailActivity.this, seasonTranslationModel);
    }
}
