package com.sevenander.tvmaniac.presentation.view.adapter.callbacks;

import android.view.View;

import com.sevenander.tvmaniac.presentation.model.TvShowModel;

public interface TvShowItemClickListener {
    void onItemClick(TvShowModel tvShowModel);

    void onItemMoreClick(View view, int position);
}
