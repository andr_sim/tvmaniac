package com.sevenander.tvmaniac.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.HasComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.DaggerEpisodeComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.EpisodeComponent;
import com.sevenander.tvmaniac.presentation.model.Translation;
import com.sevenander.tvmaniac.presentation.model.VideoQuality;
import com.sevenander.tvmaniac.presentation.view.adapter.TranslationPagerAdapter;
import com.sevenander.tvmaniac.util.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TranslationActivity extends BaseActivity implements HasComponent<EpisodeComponent> {

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.tl_qualities) TabLayout tlQualities;
    @Bind(R.id.vp_episodes) ViewPager vpEpisodes;

    private TranslationPagerAdapter adapter;

    private EpisodeComponent episodeComponent;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, TranslationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation);
        ButterKnife.bind(this);
        initializeInjector();
        setupToolbar();
        setupPager();
    }

    private void initializeInjector() {
        this.episodeComponent = DaggerEpisodeComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public EpisodeComponent getComponent() {
        return episodeComponent;
    }

    private void setupToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void setupPager() {
        Translation translation = new Translation();

        final List<VideoQuality> qualities = new ArrayList<>();
        qualities.add(VideoQuality.QUALITY_1080P);
        qualities.add(VideoQuality.QUALITY_720P);
        qualities.add(VideoQuality.QUALITY_HDRIP);
        qualities.add(VideoQuality.QUALITY_360P);
        qualities.add(VideoQuality.QUALITY_SD);

        translation.setQualities(qualities);

        adapter = new TranslationPagerAdapter(getSupportFragmentManager(), translation);
        vpEpisodes.setAdapter(adapter);
        tlQualities.setupWithViewPager(vpEpisodes);

        vpEpisodes.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                VideoQuality quality = qualities.get(position);
                changeColors(
                        Utils.getQualityColor(quality),
                        Utils.getQualityColorDark(quality));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void changeColors(int colorId, int darkColorId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(darkColorId));
        }

        toolbar.setBackgroundColor(getResources().getColor(colorId));
        tlQualities.setBackgroundColor(getResources().getColor(colorId));
    }
}
