package com.sevenander.tvmaniac.presentation.model;

/**
 * Created by andrii on 21.10.16.
 */
public enum VideoQuality {
    QUALITY_1080P,
    QUALITY_720P,
    QUALITY_480P,
    QUALITY_360P,
    QUALITY_HDRIP,
    QUALITY_HDTVRIP,
    QUALITY_WEBDL,
    QUALITY_SD,
    QUALITY_UNKNOWN
}
