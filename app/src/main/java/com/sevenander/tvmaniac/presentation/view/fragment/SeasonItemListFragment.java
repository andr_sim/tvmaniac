package com.sevenander.tvmaniac.presentation.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.components.SeasonItemComponent;
import com.sevenander.tvmaniac.presentation.model.SeasonItemModel;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.presenter.SeasonItemListPresenter;
import com.sevenander.tvmaniac.presentation.view.SeasonItemListView;
import com.sevenander.tvmaniac.presentation.view.adapter.SeasonTranslationAdapter;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TranslationItemClickListener;
import com.sevenander.tvmaniac.presentation.view.fragment.callbacks.SeasonTranslationListListener;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SeasonItemListFragment extends BaseFragment implements TranslationItemClickListener,
        SeasonItemListView {

    @Inject SeasonItemListPresenter seasonItemListPresenter;
    @Inject SeasonTranslationAdapter adapter;

    @Bind(R.id.b_season_items_retry) Button bRetry;
    @Bind(R.id.pb_season_items) ProgressBar pbSeasonItems;
    @Bind(R.id.rv_season_items) RecyclerView rvSeasonItems;

    private SeasonTranslationListListener seasonTranslationListListener;

    public SeasonItemListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SeasonTranslationListListener) {
            this.seasonTranslationListListener = (SeasonTranslationListListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(SeasonItemComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_season_item_list, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.seasonItemListPresenter.setView(this);
        if (savedInstanceState == null) {
            loadTvShowList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.seasonItemListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.seasonItemListPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rvSeasonItems.setAdapter(null);
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.seasonItemListPresenter.destroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.seasonTranslationListListener = null;
    }

    @Override
    public void showLoading() {
        pbSeasonItems.setVisibility(View.VISIBLE);
        getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        pbSeasonItems.setVisibility(View.GONE);
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
        bRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        bRetry.setVisibility(View.GONE);
    }

    @Override
    public void renderSeasonItemList(Collection<SeasonItemModel> seasonItemModels) {
        if (seasonItemModels != null)
            adapter.setTvShowModels(seasonItemModels);
    }

    @Override
    public void viewSeasonTranslation(SeasonTranslationModel seasonTranslationModel) {
        if (this.seasonTranslationListListener != null)
            this.seasonTranslationListListener.onSeasonTranslationClicked(seasonTranslationModel);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    private void setupViews() {
        adapter.setOnItemClickListener(SeasonItemListFragment.this);
        rvSeasonItems.setHasFixedSize(true);
        rvSeasonItems.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        rvSeasonItems.setLayoutManager(layoutManager);
        rvSeasonItems.setAdapter(adapter);
    }

    /**
     * Loads all tv shows.
     */
    private void loadTvShowList() {
        this.seasonItemListPresenter.initialize();
    }

    @OnClick(R.id.b_season_items_retry)
    public void onButtonRetryClick() {
        loadTvShowList();
    }

    @Override
    public void onItemClick(SeasonTranslationModel seasonTranslationModel) {
        if (seasonItemListPresenter != null && seasonTranslationModel != null)
            seasonItemListPresenter.onSeasonTranslationClicked(seasonTranslationModel);
    }
}
