package com.sevenander.tvmaniac.presentation.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.components.EpisodeComponent;
import com.sevenander.tvmaniac.presentation.model.EpisodeModel;
import com.sevenander.tvmaniac.presentation.presenter.EpisodeListPresenter;
import com.sevenander.tvmaniac.presentation.view.EpisodeListView;
import com.sevenander.tvmaniac.presentation.view.adapter.EpisodeAdapter;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.EpisodeItemClickListener;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EpisodeListFragment extends BaseFragment implements EpisodeItemClickListener,
        EpisodeListView {

    @Inject EpisodeListPresenter episodeListPresenter;
    @Inject EpisodeAdapter adapter;

    @Bind(R.id.pb_episodes) ProgressBar pbEpisodes;
    @Bind(R.id.rv_episodes) RecyclerView rvEpisodes;

    public EpisodeListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(EpisodeComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_episode_list, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.episodeListPresenter.setView(this);
        if (savedInstanceState == null) {
            loadUserList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.episodeListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.episodeListPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rvEpisodes.setAdapter(null);
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.episodeListPresenter.destroy();
    }

    @Override
    public void showLoading() {
        pbEpisodes.setVisibility(View.VISIBLE);
        getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        pbEpisodes.setVisibility(View.GONE);
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
//        this.rl_retry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
//        this.rl_retry.setVisibility(View.GONE);
    }

    @Override
    public void renderEpisodeList(Collection<EpisodeModel> episodeModelCollection) {
        if (episodeModelCollection != null) {
            adapter.setEpisodeModels(episodeModelCollection);
        }
    }

    @Override
    public void viewEpisode(EpisodeModel userModel) {
//        if (this.userListListener != null) {
//            this.userListListener.onUserClicked(userModel);
//        }
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    private void setupViews() {
        adapter.setOnItemClickListener(EpisodeListFragment.this);
        rvEpisodes.setHasFixedSize(true);
        rvEpisodes.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        rvEpisodes.setLayoutManager(layoutManager);
        rvEpisodes.setAdapter(adapter);
    }

    /**
     * Loads all tvShows.
     */
    private void loadUserList() {
        this.episodeListPresenter.initialize();
    }

//    @OnClick(R.id.bt_retry)
//    void onButtonRetryClick() {
//        loadUserList();
//    }

    @Override
    public void onItemClick(int position) {
        // episodeModel get by pos
//        if (seasonItemListPresenter != null && episodeModel != null) {
//            seasonItemListPresenter.onUserClicked(episodeModel);
//        }
    }

    @Override
    public void onItemDownloadClick(int position) {

    }
}
