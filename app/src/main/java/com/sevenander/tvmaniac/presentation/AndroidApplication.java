package com.sevenander.tvmaniac.presentation;

import android.app.Application;

import com.sevenander.tvmaniac.presentation.internal.di.components.ApplicationComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.DaggerApplicationComponent;
import com.sevenander.tvmaniac.presentation.internal.di.modules.ApplicationModule;

/**
 * Android Main Application
 */
public class AndroidApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
//        this.initializeLeakDetection();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

//    private void initializeLeakDetection() {
//        if (BuildConfig.DEBUG) {
//            LeakCanary.install(this);
//        }
//    }
}
