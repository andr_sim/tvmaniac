package com.sevenander.tvmaniac.presentation.view;

import com.sevenander.tvmaniac.presentation.model.EpisodeModel;

import java.util.Collection;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a list of {@link EpisodeModel}.
 */
public interface EpisodeListView extends LoadDataView {
    /**
     * Render a episode list in the UI.
     *
     * @param episodeModelCollection The collection of {@link EpisodeModel} that will be shown.
     */
    void renderEpisodeList(Collection<EpisodeModel> episodeModelCollection);

    /**
     * View a {@link EpisodeModel} details.
     *
     * @param episodeModel The episode that will be shown.
     */
    void viewEpisode(EpisodeModel episodeModel);
}
