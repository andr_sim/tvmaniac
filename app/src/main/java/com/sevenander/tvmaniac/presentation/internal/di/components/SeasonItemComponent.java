package com.sevenander.tvmaniac.presentation.internal.di.components;

import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.internal.di.modules.ActivityModule;
import com.sevenander.tvmaniac.presentation.internal.di.modules.SeasonItemModule;
import com.sevenander.tvmaniac.presentation.view.fragment.SeasonItemListFragment;

import dagger.Component;

/**
 * A scope {@link PerActivity} component.
 * Injects season item specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, SeasonItemModule.class})
public interface SeasonItemComponent {
    void inject(SeasonItemListFragment seasonItemListFragment);
}
