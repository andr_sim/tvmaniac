package com.sevenander.tvmaniac.presentation.model;

/**
 * Class that represents a Episode in the presentation layer.
 */
public class EpisodeModel {

    private String id;
    private int episodeNumber;
    private String directFileUrl;
    private String directFileName;
    private String directFileSize;
    private VideoQuality quality;
    private String requestLink;
    private String streamLink;

    public EpisodeModel() {
    }

    public EpisodeModel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    public String getDirectFileUrl() {
        return directFileUrl;
    }

    public void setDirectFileUrl(String directFileUrl) {
        this.directFileUrl = directFileUrl;
    }

    public String getDirectFileName() {
        return directFileName;
    }

    public void setDirectFileName(String directFileName) {
        this.directFileName = directFileName;
    }

    public String getDirectFileSize() {
        return directFileSize;
    }

    public void setDirectFileSize(String directFileSize) {
        this.directFileSize = directFileSize;
    }

    public VideoQuality getQuality() {
        return quality;
    }

    public void setQuality(VideoQuality quality) {
        this.quality = quality;
    }

    public String getRequestLink() {
        return requestLink;
    }

    public void setRequestLink(String requestLink) {
        this.requestLink = requestLink;
    }

    public String getStreamLink() {
        return streamLink;
    }

    public void setStreamLink(String streamLink) {
        this.streamLink = streamLink;
    }
}
