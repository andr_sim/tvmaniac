package com.sevenander.tvmaniac.presentation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.SeasonItemModel;
import com.sevenander.tvmaniac.presentation.model.SeasonModel;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TranslationItemClickListener;
import com.sevenander.tvmaniac.presentation.view.adapter.holders.SeasonViewHolder;
import com.sevenander.tvmaniac.presentation.view.adapter.holders.TranslationViewHolder;
import com.sevenander.tvmaniac.util.Utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class SeasonTranslationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    private List<SeasonItemModel> seasonItemModels;
    private final LayoutInflater layoutInflater;

    private TranslationItemClickListener listener;

    @Inject
    public SeasonTranslationAdapter(Context context) {
        this.context = context;
        this.seasonItemModels = Collections.emptyList();
        this.layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return (seasonItemModels != null) ? seasonItemModels.size() : 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        int layoutId = viewType == SeasonItemModel.TYPE_ITEM_TOP ?
                R.layout.item_season : R.layout.item_translation;

        View view = layoutInflater.inflate(layoutId, parent, false);

        if (viewType == SeasonItemModel.TYPE_ITEM_TOP)
            return new SeasonViewHolder(view);
        else
            return new TranslationViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == SeasonItemModel.TYPE_ITEM_TOP) {
            bindSeason((SeasonViewHolder) holder, position);
        } else {
            bindTranslation((TranslationViewHolder) holder, position);
        }
    }

    private void bindTranslation(TranslationViewHolder holder, int position) {
        SeasonTranslationModel seasonTranslationModel =
                (SeasonTranslationModel) seasonItemModels.get(position);
        holder.setSeasonTranslationModel(seasonTranslationModel);
        holder.ivFlag.setImageDrawable(
                context.getResources().getDrawable(
                        Utils.getDrawableFlag(seasonTranslationModel.getLanguage())));
        holder.tvTitle.setText(seasonTranslationModel.getTitle());
        holder.tvInfo.setText(String.format("%s episodes · %s",
                seasonTranslationModel.getEpisodeCount(),
                seasonTranslationModel.getLastUpdate()));
    }

    private void bindSeason(SeasonViewHolder holder, int position) {
        SeasonModel seasonModel = (SeasonModel) seasonItemModels.get(position);
        holder.tvTitle.setText(seasonModel.getTitle());
    }

    @Override
    public int getItemViewType(int position) {
        return seasonItemModels.get(position).getType();
    }

    public void setTvShowModels(Collection<SeasonItemModel> seasonItemModels) {
        validateEpisodesCollection(seasonItemModels);
        this.seasonItemModels = (List<SeasonItemModel>) seasonItemModels;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(TranslationItemClickListener listener) {
        this.listener = listener;
    }

    private void validateEpisodesCollection(Collection<SeasonItemModel> episodeModels) {
        if (episodeModels == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }
}
