package com.sevenander.tvmaniac.presentation.view;

import com.sevenander.tvmaniac.presentation.model.SeasonItemModel;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;

import java.util.Collection;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 * In this case is used as a view representing a list of {@link SeasonItemModel}.
 */
public interface SeasonItemListView extends LoadDataView {
    /**
     * Render a season items list in the UI.
     *
     * @param seasonItemModels The collection of {@link SeasonItemModel} that will be shown.
     */
    void renderSeasonItemList(Collection<SeasonItemModel> seasonItemModels);

    /**
     * View a {@link SeasonTranslationModel} details.
     *
     * @param seasonTranslationModel The season translation that will be shown.
     */
    void viewSeasonTranslation(SeasonTranslationModel seasonTranslationModel);
}
