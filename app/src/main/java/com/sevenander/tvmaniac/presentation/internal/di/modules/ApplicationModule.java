package com.sevenander.tvmaniac.presentation.internal.di.modules;

import android.content.Context;

import com.sevenander.tvmaniac.data.cache.TvShowCache;
import com.sevenander.tvmaniac.data.cache.TvShowCacheImpl;
import com.sevenander.tvmaniac.data.executor.JobExecutor;
import com.sevenander.tvmaniac.data.repository.TvShowDataRepository;
import com.sevenander.tvmaniac.domain.executor.PostExecutionThread;
import com.sevenander.tvmaniac.domain.executor.ThreadExecutor;
import com.sevenander.tvmaniac.domain.repository.TvShowRepository;
import com.sevenander.tvmaniac.presentation.AndroidApplication;
import com.sevenander.tvmaniac.presentation.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
    private final AndroidApplication application;

    public ApplicationModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    TvShowCache provideUserCache(TvShowCacheImpl tvShowCache) {
        return tvShowCache;
    }

    @Provides
    @Singleton
    TvShowRepository provideTvShowRepository(TvShowDataRepository tvShowDataRepository) {
        return tvShowDataRepository;
    }
}
