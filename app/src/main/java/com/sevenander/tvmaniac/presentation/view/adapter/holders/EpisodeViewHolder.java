package com.sevenander.tvmaniac.presentation.view.adapter.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.EpisodeItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EpisodeViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.tv_episode_title) public TextView tvTitle;
    @Bind(R.id.tv_episode_file) public TextView tvFile;
    @Bind(R.id.tv_episode_quality) public TextView tvQuality;

    private EpisodeItemClickListener callback;

    public EpisodeViewHolder(View itemView, EpisodeItemClickListener callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.callback = callback;
    }

    @OnClick(R.id.rl_episode_item)
    public void onItemClick() {
        if (callback != null)
            callback.onItemClick(getAdapterPosition());
    }

    @OnClick(R.id.iv_download)
    public void onItemDownloadClick() {
        if (callback != null)
            callback.onItemDownloadClick(getAdapterPosition());
    }
}
