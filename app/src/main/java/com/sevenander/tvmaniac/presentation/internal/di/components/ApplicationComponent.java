package com.sevenander.tvmaniac.presentation.internal.di.components;

import android.content.Context;

import com.sevenander.tvmaniac.domain.executor.PostExecutionThread;
import com.sevenander.tvmaniac.domain.executor.ThreadExecutor;
import com.sevenander.tvmaniac.domain.repository.TvShowRepository;
import com.sevenander.tvmaniac.presentation.internal.di.modules.ApplicationModule;
import com.sevenander.tvmaniac.presentation.view.activity.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(BaseActivity baseActivity);

    //Exposed to sub-graphs.
    Context context();

    ThreadExecutor threadExecutor();

    PostExecutionThread postExecutionThread();

    TvShowRepository tvShowRepository();
}
