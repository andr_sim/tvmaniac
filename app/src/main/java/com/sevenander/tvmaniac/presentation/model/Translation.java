package com.sevenander.tvmaniac.presentation.model;

import java.util.Date;
import java.util.List;

/**
 * Created by andrii on 21.10.16.
 */
public class Translation {

    private String id;
    private String title;
    private String subtitle;
    private Date lastUpdate;
    private List<String> episodes;
    private List<VideoQuality> qualities;
    private String language;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<String> getEpisodes() {
        return episodes;
    }

    public void setEpisodes(List<String> episodes) {
        this.episodes = episodes;
    }

    public List<VideoQuality> getQualities() {
        return qualities;
    }

    public void setQualities(List<VideoQuality> qualities) {
        this.qualities = qualities;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
