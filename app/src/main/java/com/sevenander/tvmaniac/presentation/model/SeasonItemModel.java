package com.sevenander.tvmaniac.presentation.model;

/**
 * Created by andrii on 21.10.16.
 */
public abstract class SeasonItemModel {

    public static final int TYPE_ITEM_TOP = 1;
    public static final int TYPE_ITEM_DEFAULT = 2;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public abstract int getType();
}
