package com.sevenander.tvmaniac.presentation.view.adapter.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TranslationItemClickListener;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TranslationViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.iv_flag_icon) public ImageView ivFlag;
    @Bind(R.id.tv_translation_title) public TextView tvTitle;
    @Bind(R.id.tv_translation_info) public TextView tvInfo;

    private TranslationItemClickListener callback;

    private SeasonTranslationModel seasonTranslationModel;

    public TranslationViewHolder(View itemView, TranslationItemClickListener callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.callback = callback;
    }

    public void setSeasonTranslationModel(SeasonTranslationModel seasonTranslationModel) {
        this.seasonTranslationModel = seasonTranslationModel;
    }

    @OnClick(R.id.rl_translation_item)
    public void onItemClick() {
        if (callback != null)
            callback.onItemClick(seasonTranslationModel);
    }
}
