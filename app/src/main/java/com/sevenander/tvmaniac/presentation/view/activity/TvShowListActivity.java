package com.sevenander.tvmaniac.presentation.view.activity;

import android.os.Bundle;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.HasComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.DaggerTvShowComponent;
import com.sevenander.tvmaniac.presentation.internal.di.components.TvShowComponent;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.view.fragment.TvShowListFragment;
import com.sevenander.tvmaniac.presentation.view.fragment.callbacks.TvShowListListener;

public class TvShowListActivity extends BaseActivity implements HasComponent<TvShowComponent>,
        TvShowListListener {

    private TvShowComponent tvShowComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_show_list);
        initializeInjector();
        replaceFragment(R.id.rl_container, new TvShowListFragment());

//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//
//                try {
////                    Logger.d(Jsoup.connect("http://fs.to/video/serials/ix537ZB6OWOQj0P2Yue4ZW-gosudarstvennyj-sekretar.html").get().toString());
//                    Logger.d(Jsoup.connect("http://fs.to/video/serials/ix537ZB6OWOQj0P2Yue4ZW-gosudarstvennyj-sekretar.html?ajax&r=0.7816448514349759&id=x537ZB6OWOQj0P2Yue4ZW&download=1&view=1&view_embed=0&blocked=0&frame_hash=1ktpalw&folder_quality=null&folder_lang=null&folder_translate=null&folder=0&_=1477930595433").get().toString());
//                } catch (IOException e) {
//                    Logger.e(e.getMessage(), e);
//                }
//
//                return null;
//            }
//        }.execute();
    }

    private void initializeInjector() {
        this.tvShowComponent = DaggerTvShowComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public TvShowComponent getComponent() {
        return tvShowComponent;
    }

    @Override
    public void onTvShowClicked(TvShowModel tvShowModel) {
        navigator.navigateToTvShowDetails(TvShowListActivity.this, tvShowModel);
    }
}
