package com.sevenander.tvmaniac.presentation.internal.di.modules;

import dagger.Module;

/**
 * Dagger module that provides episode related collaborators.
 */
@Module
public class EpisodeModule {

    private int userId = -1;

    public EpisodeModule() {
    }

    public EpisodeModule(int userId) {
        this.userId = userId;
    }

//  @Provides @PerActivity @Named("userList") UseCase provideGetUserListUseCase(
//      GetUserList getUserList) {
//    return getUserList;
//  }
//
//  @Provides @PerActivity @Named("userDetails") UseCase provideGetUserDetailsUseCase(
//      UserRepository userRepository, ThreadExecutor threadExecutor,
//      PostExecutionThread postExecutionThread) {
//    return new GetUserDetails(userId, userRepository, threadExecutor, postExecutionThread);
//  }
}