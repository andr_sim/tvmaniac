package com.sevenander.tvmaniac.presentation.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.sevenander.tvmaniac.presentation.model.Translation;
import com.sevenander.tvmaniac.presentation.view.fragment.EpisodeListFragment;

public class TranslationPagerAdapter extends FragmentPagerAdapter {

    private Translation translation;

    public TranslationPagerAdapter(FragmentManager fragmentManager, Translation translation) {
        super(fragmentManager);
        this.translation = translation;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return translation.getQualities().get(position).name().replace("QUALITY_", "");
    }

    @Override
    public int getCount() {
        return translation.getQualities().size();
    }

    @Override
    public Fragment getItem(int position) {
        return new EpisodeListFragment();
    }
}
