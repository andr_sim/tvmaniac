package com.sevenander.tvmaniac.presentation.view.activity;

import android.content.Intent;
import android.os.Bundle;

import com.sevenander.tvmaniac.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        startActivity(new Intent(MainActivity.this, TranslationActivity.class));
        startActivity(new Intent(MainActivity.this, TvShowListActivity.class));
//        startActivity(new Intent(MainActivity.this, TvShowNewActivity.class));
    }
}
