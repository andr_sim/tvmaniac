/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sevenander.tvmaniac.presentation.presenter;

//import android.support.annotation.NonNull;
//import com.fernandocejas.android10.sample.domain.exception.DefaultErrorBundle;
//import com.fernandocejas.android10.sample.domain.interactor.DefaultSubscriber;
//import com.fernandocejas.android10.sample.domain.interactor.UseCase;

import android.support.annotation.NonNull;

import com.sevenander.tvmaniac.domain.Episode;
import com.sevenander.tvmaniac.domain.exception.ErrorBundle;
import com.sevenander.tvmaniac.presentation.exception.ErrorMessageFactory;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.mapper.EpisodeModelDataMapper;
import com.sevenander.tvmaniac.presentation.model.EpisodeModel;
import com.sevenander.tvmaniac.presentation.model.VideoQuality;
import com.sevenander.tvmaniac.presentation.view.EpisodeListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class EpisodeListPresenter implements Presenter {

    private EpisodeListView viewListView;

    //  private final UseCase getUserListUseCase;
    private final EpisodeModelDataMapper userModelDataMapper;

    @Inject
    public EpisodeListPresenter(
//            @Named("episodeList") UseCase getUserListUserCase,
//    public EpisodeListPresenter(@Named("userList") UseCase getUserListUserCase,
            EpisodeModelDataMapper userModelDataMapper) {
//    this.getUserListUseCase = getUserListUserCase;
        this.userModelDataMapper = userModelDataMapper;
    }

    public void setView(@NonNull EpisodeListView view) {
        this.viewListView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
//    this.getUserListUseCase.unsubscribe();
        this.viewListView = null;
    }

    /**
     * Initializes the presenter by start retrieving the tvShow list.
     */
    public void initialize() {
        this.loadUserList();
    }

    /**
     * Loads all tvShows.
     */
    private void loadUserList() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getUserList();
    }

    public void onUserClicked(EpisodeModel episodeModel) {
        this.viewListView.viewEpisode(episodeModel);
    }

    private void showViewLoading() {
        this.viewListView.showLoading();
    }

    private void hideViewLoading() {
        this.viewListView.hideLoading();
    }

    private void showViewRetry() {
        this.viewListView.showRetry();
    }

    private void hideViewRetry() {
        this.viewListView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
                errorBundle.getException());
        this.viewListView.showError(errorMessage);
    }

    private void showUsersCollectionInView(Collection<Episode> usersCollection) {
        final Collection<EpisodeModel> userModelsCollection =
                this.userModelDataMapper.transform(usersCollection);
        this.viewListView.renderEpisodeList(userModelsCollection);
    }

    private void getUserList() {
//    this.getUserListUseCase.execute(new UserListSubscriber());
        hideViewLoading();
        showUsersCollectionInView(mockEpisodeList());
    }

    private List<Episode> mockEpisodeList() {
        List<Episode> episodeModels = new ArrayList<>();

        Episode episode = new Episode();
        episode.setEpisodeNumber(1);
        episode.setDirectFileName("American.Horror.Story.S01E01.720p.Rus[LostFilm].mkv");
        episode.setDirectFileSize("1.93 GB");
        episode.setQuality(VideoQuality.QUALITY_720P);
        episodeModels.add(episode);

        episode = new Episode();
        episode.setEpisodeNumber(2);
        episode.setDirectFileName("American.Horror.Story.S01E02.720p.Rus[LostFilm].mkv");
        episode.setDirectFileSize("1.59 GB");
        episode.setQuality(VideoQuality.QUALITY_1080P);
        episodeModels.add(episode);

        episode = new Episode();
        episode.setEpisodeNumber(3);
        episode.setDirectFileName("American.Horror.Story.S01E03.720p.Rus[LostFilm].mkv");
        episode.setDirectFileSize("1.69 GB");
        episode.setQuality(VideoQuality.QUALITY_HDRIP);
        episodeModels.add(episode);

        episode = new Episode();
        episode.setEpisodeNumber(4);
        episode.setDirectFileName("American.Horror.Story.S01E04.720p.Rus[LostFilm].mkv");
        episode.setDirectFileSize("1.22 GB");
        episode.setQuality(VideoQuality.QUALITY_360P);
        episodeModels.add(episode);

        episode = new Episode();
        episode.setEpisodeNumber(5);
        episode.setDirectFileName("American.Horror.Story.S01E05.720p.Rus[LostFilm].mkv");
        episode.setDirectFileSize("1.39 GB");
        episode.setQuality(VideoQuality.QUALITY_UNKNOWN);
        episodeModels.add(episode);

//        Collections.sort(lessons, new LessonComparator());
        return episodeModels;
    }

//
//  private final class UserListSubscriber extends DefaultSubscriber<List<User>> {
//
//    @Override public void onCompleted() {
//      UserListPresenter.this.hideViewLoading();
//    }
//
//    @Override public void onError(Throwable e) {
//      UserListPresenter.this.hideViewLoading();
//      UserListPresenter.this.showErrorMessage(new DefaultErrorBundle((Exception) e));
//      UserListPresenter.this.showViewRetry();
//    }
//
//    @Override public void onNext(List<User> tvShows) {
//      UserListPresenter.this.showTvShowsCollectionInView(tvShows);
//    }
//  }
}
