package com.sevenander.tvmaniac.presentation.presenter;

import android.support.annotation.NonNull;

import com.sevenander.tvmaniac.domain.Season;
import com.sevenander.tvmaniac.domain.SeasonItem;
import com.sevenander.tvmaniac.domain.SeasonTranslation;
import com.sevenander.tvmaniac.domain.exception.ErrorBundle;
import com.sevenander.tvmaniac.presentation.exception.ErrorMessageFactory;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.mapper.SeasonItemModelDataMapper;
import com.sevenander.tvmaniac.presentation.model.SeasonItemModel;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.model.TranslationLanguage;
import com.sevenander.tvmaniac.presentation.view.SeasonItemListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 * {@link Presenter} that controls communication between views and models of the presentation
 * layer.
 */
@PerActivity
public class SeasonItemListPresenter implements Presenter {

    private SeasonItemListView viewListView;

    private final SeasonItemModelDataMapper seasonItemModelDataMapper;

    @Inject
    public SeasonItemListPresenter(SeasonItemModelDataMapper seasonItemModelDataMapper) {
        this.seasonItemModelDataMapper = seasonItemModelDataMapper;
    }

    public void setView(@NonNull SeasonItemListView view) {
        this.viewListView = view;
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        this.viewListView = null;
    }

    /**
     * Initializes the presenter by start retrieving the tvShow list.
     */
    public void initialize() {
        this.loadTvShowList();
    }

    /**
     * Loads all tvShows.
     */
    private void loadTvShowList() {
        this.hideViewRetry();
        this.showViewLoading();
        this.getTvShowUserList();
    }

    public void onSeasonTranslationClicked(SeasonTranslationModel seasonTranslationModel) {
        this.viewListView.viewSeasonTranslation(seasonTranslationModel);
    }

    private void showViewLoading() {
        this.viewListView.showLoading();
    }

    private void hideViewLoading() {
        this.viewListView.hideLoading();
    }

    private void showViewRetry() {
        this.viewListView.showRetry();
    }

    private void hideViewRetry() {
        this.viewListView.hideRetry();
    }

    private void showErrorMessage(ErrorBundle errorBundle) {
        String errorMessage = ErrorMessageFactory.create(this.viewListView.context(),
                errorBundle.getException());
        this.viewListView.showError(errorMessage);
    }

    private void showUsersCollectionInView(Collection<SeasonItem> seasonItems) {
        final Collection<SeasonItemModel> seasonItemModels =
                this.seasonItemModelDataMapper.transform(seasonItems);
        this.viewListView.renderSeasonItemList(seasonItemModels);
    }

    private void getTvShowUserList() {
        hideViewLoading();
        showUsersCollectionInView(mockList());
    }

    private List<SeasonItem> mockList() {
        List<SeasonItem> items = new ArrayList<>();

        Season season = new Season();
        season.setTitle("Season 1");
        items.add(season);

        SeasonTranslation translation = new SeasonTranslation();
        translation.setTitle("Lostfilm");
        translation.setEpisodeCount(4);
        translation.setLanguage(TranslationLanguage.RUS);
        translation.setLastUpdate(new Date());
        items.add(translation);

        translation = new SeasonTranslation();
        translation.setTitle("Baibako");
        translation.setEpisodeCount(4);
        translation.setLanguage(TranslationLanguage.UKR);
        translation.setLastUpdate(new Date());
        items.add(translation);

        season = new Season();
        season.setTitle("Season 2");
        items.add(season);

        translation = new SeasonTranslation();
        translation.setTitle("Original");
        translation.setEpisodeCount(20);
        translation.setLanguage(TranslationLanguage.ENG);
        translation.setLastUpdate(new Date());
        items.add(translation);

        season = new Season();
        season.setTitle("Season 3");
        items.add(season);

        translation = new SeasonTranslation();
        translation.setTitle("Lostfilm");
        translation.setEpisodeCount(20);
        translation.setLanguage(TranslationLanguage.RUS);
        translation.setLastUpdate(new Date());
        items.add(translation);

        translation = new SeasonTranslation();
        translation.setTitle("Baibako");
        translation.setEpisodeCount(20);
        translation.setLanguage(TranslationLanguage.UKR);
        translation.setLastUpdate(new Date());
        items.add(translation);

        translation = new SeasonTranslation();
        translation.setTitle("Original");
        translation.setEpisodeCount(20);
        translation.setLanguage(TranslationLanguage.ENG);
        translation.setLastUpdate(new Date());
        items.add(translation);

        items.add(new Season());

//        Collections.sort(lessons, new LessonComparator());

        return items;
    }
}
