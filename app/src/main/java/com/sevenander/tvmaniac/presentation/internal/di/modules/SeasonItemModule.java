package com.sevenander.tvmaniac.presentation.internal.di.modules;

import dagger.Module;

/**
 * Dagger module that provides season item related collaborators.
 */
@Module
public class SeasonItemModule {

    public SeasonItemModule() {
    }
}
