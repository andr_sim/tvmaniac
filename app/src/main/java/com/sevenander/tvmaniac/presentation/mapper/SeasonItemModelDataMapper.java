package com.sevenander.tvmaniac.presentation.mapper;

import com.sevenander.tvmaniac.domain.Season;
import com.sevenander.tvmaniac.domain.SeasonItem;
import com.sevenander.tvmaniac.domain.SeasonTranslation;
import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.model.SeasonItemModel;
import com.sevenander.tvmaniac.presentation.model.SeasonModel;
import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.inject.Inject;

/**
 * Mapper class used to transform {@link SeasonItem} (in the domain layer)
 * to {@link SeasonItemModel} in the presentation layer.
 */
@PerActivity
public class SeasonItemModelDataMapper {

    @Inject
    public SeasonItemModelDataMapper() {
    }

    /**
     * Transform a {@link Season} into an {@link SeasonModel}.
     *
     * @param season Object to be transformed.
     * @return {@link SeasonModel}.
     */
    public SeasonModel transform(Season season) {
        if (season == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SeasonModel seasonModel = new SeasonModel();
        seasonModel.setTitle(season.getTitle());

        return seasonModel;
    }

    /**
     * Transform a {@link SeasonTranslation} into an {@link SeasonTranslationModel}.
     *
     * @param seasonTranslation Object to be transformed.
     * @return {@link SeasonTranslationModel}.
     */
    public SeasonTranslationModel transform(SeasonTranslation seasonTranslation) {
        if (seasonTranslation == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        SeasonTranslationModel seasonModel = new SeasonTranslationModel();
        seasonModel.setTitle(seasonTranslation.getTitle());
        seasonModel.setEpisodeCount(seasonTranslation.getEpisodeCount());
        seasonModel.setLanguage(seasonTranslation.getLanguage());
        seasonModel.setLastUpdate(seasonTranslation.getLastUpdate());

        return seasonModel;
    }

    /**
     * Transform a Collection of {@link SeasonItem} into a Collection of {@link SeasonItemModel}.
     *
     * @param seasonItems Objects to be transformed.
     * @return List of {@link SeasonItemModel}.
     */
    public Collection<SeasonItemModel> transform(Collection<SeasonItem> seasonItems) {
        Collection<SeasonItemModel> seasonItemModels;

        if (seasonItems != null && !seasonItems.isEmpty()) {
            seasonItemModels = new ArrayList<>();
            for (SeasonItem seasonItem : seasonItems) {
                if (seasonItem instanceof Season)
                    seasonItemModels.add(transform((Season) seasonItem));
                else seasonItemModels.add(transform((SeasonTranslation) seasonItem));
            }
        } else {
            seasonItemModels = Collections.emptyList();
        }

        return seasonItemModels;
    }
}
