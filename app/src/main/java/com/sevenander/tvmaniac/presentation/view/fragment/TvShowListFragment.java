package com.sevenander.tvmaniac.presentation.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.sevenander.tvmaniac.R;
import com.sevenander.tvmaniac.presentation.internal.di.components.TvShowComponent;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.presenter.TvShowListPresenter;
import com.sevenander.tvmaniac.presentation.view.TvShowListView;
import com.sevenander.tvmaniac.presentation.view.adapter.TvShowAdapter;
import com.sevenander.tvmaniac.presentation.view.adapter.callbacks.TvShowItemClickListener;
import com.sevenander.tvmaniac.presentation.view.fragment.callbacks.TvShowListListener;

import java.util.Collection;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TvShowListFragment extends BaseFragment implements TvShowItemClickListener,
        TvShowListView {

    @Inject TvShowListPresenter tvShowListPresenter;
    @Inject TvShowAdapter adapter;

    @Bind(R.id.b_tv_shows_retry) Button bRetry;
    @Bind(R.id.pb_tv_shows) ProgressBar pbTvShows;
    @Bind(R.id.rv_tv_shows) RecyclerView rvTvShows;

    private TvShowListListener tvShowListListener;

    public TvShowListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TvShowListListener) {
            this.tvShowListListener = (TvShowListListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getComponent(TvShowComponent.class).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv_show_list, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.tvShowListPresenter.setView(this);
        if (savedInstanceState == null) {
            loadTvShowList();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.tvShowListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        this.tvShowListPresenter.pause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        rvTvShows.setAdapter(null);
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.tvShowListPresenter.destroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.tvShowListListener = null;
    }

    @Override
    public void showLoading() {
        pbTvShows.setVisibility(View.VISIBLE);
        getActivity().setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void hideLoading() {
        pbTvShows.setVisibility(View.GONE);
        getActivity().setProgressBarIndeterminateVisibility(false);
    }

    @Override
    public void showRetry() {
        bRetry.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
        bRetry.setVisibility(View.GONE);
    }

    @Override
    public void renderTvShowList(Collection<TvShowModel> episodeModelCollection) {
        if (episodeModelCollection != null)
            adapter.setTvShowModels(episodeModelCollection);
    }

    @Override
    public void viewTvShow(TvShowModel tvShowModel) {
        if (this.tvShowListListener != null)
            this.tvShowListListener.onTvShowClicked(tvShowModel);
    }

    @Override
    public void showError(String message) {
        this.showToastMessage(message);
    }

    @Override
    public Context context() {
        return getActivity().getApplicationContext();
    }

    private void setupViews() {
        adapter.setOnItemClickListener(TvShowListFragment.this);
        rvTvShows.setHasFixedSize(true);
        rvTvShows.setItemAnimator(new DefaultItemAnimator());
        rvTvShows.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvTvShows.setAdapter(adapter);
    }

    /**
     * Loads all tv shows.
     */
    private void loadTvShowList() {
        this.tvShowListPresenter.initialize();
    }

    @OnClick(R.id.b_tv_shows_retry)
    public void onButtonRetryClick() {
        loadTvShowList();
    }

    @Override
    public void onItemClick(TvShowModel tvShowModel) {
        if (tvShowListPresenter != null && tvShowModel != null)
            tvShowListPresenter.onTvShowClicked(tvShowModel);
    }

    @Override
    public void onItemMoreClick(View view, int position) {
        moreClick(view, position);
    }

    private void moreClick(View v, final int position) {
        PopupMenu popup = new PopupMenu(getActivity(), v);
        popup.getMenuInflater().inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add_to_watch:

                        break;
                }
                return true;
            }
        });
    }
}
