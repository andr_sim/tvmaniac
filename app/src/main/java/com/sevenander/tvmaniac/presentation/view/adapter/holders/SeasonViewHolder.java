package com.sevenander.tvmaniac.presentation.view.adapter.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sevenander.tvmaniac.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SeasonViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.tv_season_title) public TextView tvTitle;

    public SeasonViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
