package com.sevenander.tvmaniac.presentation.view.adapter.callbacks;

import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;

/**
 * Created by andrii on 21.10.16.
 */
public interface TranslationItemClickListener {
    void onItemClick(SeasonTranslationModel seasonTranslationModel);
}
