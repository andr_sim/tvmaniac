package com.sevenander.tvmaniac.presentation.navigation;

import android.content.Context;
import android.content.Intent;

import com.sevenander.tvmaniac.presentation.model.SeasonTranslationModel;
import com.sevenander.tvmaniac.presentation.model.TvShowModel;
import com.sevenander.tvmaniac.presentation.view.activity.TranslationActivity;
import com.sevenander.tvmaniac.presentation.view.activity.TvShowDetailActivity;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Class used to navigate through the application.
 */
@Singleton
public class Navigator {

    @Inject
    public Navigator() {
        //empty
    }

    /**
     * Goes to the tvShow list screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
    public void navigateToEpisodeList(Context context) {
        if (context != null) {
            Intent intentToLaunch = TranslationActivity.getCallingIntent(context);
            context.startActivity(intentToLaunch);
        }
    }

    /**
     * Goes to the tv show details screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
    public void navigateToTvShowDetails(Context context, TvShowModel tvShowModel) {
        if (context != null) {
            Intent intentToLaunch = TvShowDetailActivity.getCallingIntent(context);
            intentToLaunch.putExtra("title", tvShowModel.getTitle());
            intentToLaunch.putExtra("image", tvShowModel.getPosterUrl());
            context.startActivity(intentToLaunch);
        }
    }

    /**
     * Goes to the translation details screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
    public void navigateToTranslationDetails(Context context,
                                             SeasonTranslationModel seasonTranslationModel) {
        if (context != null) {
            Intent intentToLaunch = TranslationActivity.getCallingIntent(context);
            intentToLaunch.putExtra("title", seasonTranslationModel.getTitle());
            context.startActivity(intentToLaunch);
        }
    }

    /**
     * Goes to the tvShow details screen.
     *
     * @param context A Context needed to open the destiny activity.
     */
//    public void navigateToUserDetails(Context context, int userId) {
//        if (context != null) {
//            Intent intentToLaunch = UserDetailsActivity.getCallingIntent(context, userId);
//            context.startActivity(intentToLaunch);
//        }
//    }
}
