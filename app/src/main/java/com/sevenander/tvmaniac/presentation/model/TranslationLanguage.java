package com.sevenander.tvmaniac.presentation.model;

/**
 * Created by andrii on 21.10.16.
 */
public enum TranslationLanguage {
    UKR,
    RUS,
    ENG,
    GER
}
