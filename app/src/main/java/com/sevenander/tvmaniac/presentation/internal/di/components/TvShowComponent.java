package com.sevenander.tvmaniac.presentation.internal.di.components;

import com.sevenander.tvmaniac.presentation.internal.di.PerActivity;
import com.sevenander.tvmaniac.presentation.internal.di.modules.ActivityModule;
import com.sevenander.tvmaniac.presentation.internal.di.modules.TvShowModule;
import com.sevenander.tvmaniac.presentation.view.fragment.TvShowListFragment;

import dagger.Component;

/**
 * A scope {@link PerActivity} component.
 * Injects tv show specific Fragments.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, TvShowModule.class})
public interface TvShowComponent {

    void inject(TvShowListFragment tvShowListFragment);

}
